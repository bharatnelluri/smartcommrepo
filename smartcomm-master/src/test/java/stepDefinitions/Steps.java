package stepDefinitions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.ui.Select;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pageObjects.GreenCardConfirmationPage;
import pageObjects.GreenCardSummaryPage;
import pageObjects.HomePage;
import pageObjects.PolicyDetailsPage;
import pageObjects.VehicleDetailsPage;
import pageObjects.WhereToSendGreenCardPage;
import resources.Base;
import resources.Log;


public class Steps extends Base{
	
	public static WebElement element = null;

	@Before
	public void beforeTest(Scenario scenario) {
		scenarioName = scenario.getName();	
	}
	
	@After
	public void afterTest(Scenario scenario) {
		if(scenario.getStatus().name() == "FAILED") {
			screenshot(scenario);
		}
		else {
			captureDetails();
		}
		driver.quit();
		Log.info("Browser Closed");
	}
	
	
	
	@Given("Launch the application using Mendix application URL")
	public void launch_the_application_using_valid_credentials() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		launchBrowser();
		
		FileInputStream fis=new FileInputStream(""+project_path+"/Environment Setup/Environment Details.xlsx");
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet("URL");
		url = sh.getRow(1).getCell(1).getStringCellValue();
		driver.get(url);
				
		Log.info("Application Launched");
	}

	@And("Click on Continue button for Single Order in Home page")
	public void continue_home_page() throws InterruptedException {
	    HomePage.button(driver);
	    Log.info("Continue button for Single Order is clicked");
	}
	@And("Enter the {string} in Policy Details page")
	public void continue_policy_Details_page(String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException {
	PolicyDetailsPage.textarea(driver, fieldName);
	Log.info(""+fieldName+" entered");
	}
	
	@And("Click on Search button in Policy Details page")
	public void Search_button_click_page() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.Search_button_click(driver);
	    Log.info("Search button in Policy Details page");
	}
	@And("Select the Address in Policy Details page")
	public void Select_address_policy_Details_page() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.Select_address(driver);
	    Log.info("Address is selected on in Policy Details page");
	}

	@And("Enter date picker for Start_Date_of_Insurence in Policy details Page")
	public void Start_Date_of_Insurence_page() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.Start_Date_of_Insurence(driver);
	    Log.info("Date Picker for start_date_policy is entered");
	}
	
	@And("Enter date picker for End_Date_of_Insurence in Policy details Page")
	public void End_Date_of_Insurence_page() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.End_Date_of_Insurence(driver);
	    Log.info("Date Picker for End_Date_of_Insurence is entered");
	}
	
	@And("Enter date picker for Start_Date_of_GreenCard in Policy details Page")
	public void Start_Date_of_GreenCard() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.Start_Date_of_GreenCard(driver);
	    Log.info("Date Picker for Start_Date_of_GreenCard is entered");
	}
	
	@And("Enter date picker for End_Date_of_GreenCard in Policy details Page")
	public void End_Date_of_GreenCard() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		PolicyDetailsPage.End_Date_of_GreenCard(driver);
	    Log.info("Date Picker for End_Date_of_GreenCard is entered");
	}
	
	@And("Click on Continue button in Policy details Page")
	public void continue_policy_page() throws InterruptedException {
		PolicyDetailsPage.Continue_button_Home_Page(driver);
	    Log.info("Continue button in Policy details Page is clicked");
	}	
	
	@And("Enter {string} in Vehicle details Page")
	public void continue_vehicle_Details_page(String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException {
	VehicleDetailsPage.textarea(driver, fieldName);
	Log.info(""+fieldName+" entered");
	}

	@And("Select Vehicle_Category from dropdown in Vehicle details Page")
	public void Select_vehicle_catgory_page() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		VehicleDetailsPage.Vehicle_catgory(driver);
    Log.info("Vehicle_Category is selected on in Policy Details page");
	}
	
	@And("Click on Continue in Vehicle details Page")
	public void continue_vehicle_page() throws InterruptedException {
		VehicleDetailsPage.Continue_button_Vehicle_Page(driver);
	    Log.info("Continue button in Vehicle details Page is clicked");
	}
	
	@And("Select Requestor Relationship from dropdown in Where to send GreenCard Page")
	public void Select_Requestor_Relationship() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		WhereToSendGreenCardPage.Requestor_Relationship(driver);
        Log.info("Requestor Relationship is selected on in Where To Send GreenCard page");
	}
	
	@And("Enter {string} in where to send GreenCard Page")
	public void continue_WhereToSendGreenCardPage_Details_page(String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException {
		WhereToSendGreenCardPage.textarea(driver, fieldName);
		Log.info(""+fieldName+" entered");
	}
	
	@And("Click on Continue in where to send GreenCard Page")
	public void continue_WhereToSendGreenCardPage() throws InterruptedException {
		WhereToSendGreenCardPage.continue_button_WhereToSendGreenCardPage(driver);
	    Log.info("Continue button in Where To Send GreenCard details Page is clicked");
	}
	
	@And ("Select No from dropdown in Green Card summary Page")
	public void radio_button_No() throws InterruptedException {
		GreenCardSummaryPage.radio_button_No_As_Docmosis_Doc_Generator(driver);
	    Log.info("Submit button in Green Card summary Page is clicked");
	}
	
	@And("Click on Submit Green Card Request in Green Card summary Page")
	public void submit_GreenCardSummaryPage() throws InterruptedException{
		GreenCardSummaryPage.submit_GreenCard_Button(driver);
	   Log.info("Submit Green Card Request in Green Card summary Page");
	}
	
	@And("User receives the referenceID on successfull transaction")
	 public void referenceID_GreenCardConfirmationPage() throws InterruptedException {
		GreenCardConfirmationPage.referenceId_generated(driver);
		 Log.info("User receives the referenceID on successfull transaction");
	}

	
}

	

	


