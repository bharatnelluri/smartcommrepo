package resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import cucumber.api.Scenario;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Base {
	
	public static WebDriver driver;
	public static Properties prop;
	public static BufferedReader f;
	public static String scenarioName;
	//public static String referenceID="";
	public static String referenceID;
	public static String env, url, username, password;
	public static String file_path;
//	public static String claimNo;
	
	public static String project_path = System.getProperty("user.dir");
	
	public static DateFormat dateTime = new SimpleDateFormat("dd_MM_YYYY_HHmmss");
	public static Date d = new Date();
	
	//Execution start date and time
	protected static String execution_start_date_time = dateTime.format(d);
	
	public static void config() throws IOException
	{
		f = new BufferedReader(new FileReader(""+project_path+"\\src\\test\\java\\resources\\Configuration.properties"));
		try {
				prop = new Properties();
				prop.load(f);
				f.close();
	       }
	   catch (Exception e)
	   {
		   e.printStackTrace();
	   }
	 }
	
	public void screenshot(Scenario s)
	{
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try 
		{
			FileUtils.copyFile(src, new File(""+project_path+"\\Screenshots\\"+execution_start_date_time+"\\"+scenarioName+".png"));
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public void launchBrowser() {
		if(prop.getProperty("browser").equalsIgnoreCase("chrome"))
		 {
			System.setProperty("webdriver.chrome.driver", ""+project_path+"\\Drivers\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions(); 
			options.addArguments("--no-sandbox"); // Bypass OS security model, MUST BE THE VERY FIRST OPTION
			//options.addArguments("--headless"); //Runs the script in headless mode
			options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
			options.addArguments("--window-size=1366x768");
			options.addArguments("--start-maximized"); // open Browser in maximized mode
			options.addArguments("--disable-gpu"); // applicable to windows OS only
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			driver = new ChromeDriver(options);
		 }
		 else if(prop.getProperty("browser").equalsIgnoreCase("firefox"))
		 {
			 System.setProperty("webdriver.firefox.bin", "C:\\Users\\jebakumar.j\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
			 System.setProperty("webdriver.gecko.driver", ""+project_path+"\\Drivers\\geckodriver.exe");
			 driver = new FirefoxDriver();
		 }
		 else if(prop.getProperty("browser").equalsIgnoreCase("IE"))
		 {
			 System.setProperty("webdriver.ie.driver", ""+project_path+"\\Drivers\\IEDriverServer.exe");
			 driver = new InternetExplorerDriver();
		 }
		
		driver.manage().deleteAllCookies();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		driver.manage().window().maximize();
		 
		Log.info("Browser Launched");
	}
	
	public static void isClickable(String xpath) throws InterruptedException      
	{
		int attempts = 0;
	    while(attempts < 10) {
	        try {
	            driver.findElement(By.xpath(xpath)).click();
	            break;
	        } catch(Exception e) {
	        	Thread.sleep(500);
	        }
	        attempts++;
	    }
	}
	
	public static void captureDetails() {
		try
		{
			String filename = "SmartComm_"+execution_start_date_time+"";
			Path path = Paths.get(""+project_path+"\\Output\\"+filename+".xlsx");
			if (Files.notExists(path))
			{
				Workbook wb = new XSSFWorkbook();
				Sheet sh = wb.createSheet("Results");
				FileOutputStream out = new FileOutputStream(new File(""+project_path+"\\Output\\"+filename+".xlsx"));
				String[] headers = new String[] { "S.No", "Test ID","Reference_ID"};
				Row r = sh.createRow(0);
				for (int rn=0; rn<headers.length; rn++) 
				{
					r.createCell(rn).setCellValue(headers[rn]);
				}
				wb.write(out);
				out.close();
				wb.close();
			}
			FileInputStream f=new FileInputStream(""+project_path+"\\Output\\"+filename+".xlsx");
			Workbook w = WorkbookFactory.create(f);
			Sheet s = w.getSheet("Results");
			int counter = 1;
			outputloop:
				while(counter <= 10000)
				{
					try
					{
						@SuppressWarnings("unused")
						double sno = s.getRow(counter).getCell(0).getNumericCellValue();
						counter++;				
					}
					catch (NullPointerException NPE)
					{
						Row r = s.createRow(counter);
						r.createCell(0).setCellValue(counter);
						r.createCell(1).setCellValue(scenarioName);
						r.createCell(2).setCellValue(referenceID);
						
						FileOutputStream out = new FileOutputStream(new File(""+project_path+"\\Output\\"+filename+".xlsx"));
						w.write(out);
						out.close();
						w.close();
						break outputloop;
					}
				}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.error("Error with data capture");
		}
		Log.info("Data captured in excel");
	}
		
}
