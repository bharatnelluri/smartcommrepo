package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.io.FileInputStream;
import java.io.IOException;
import resources.Base;
import resources.Log;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;


public class PolicyDetailsPage extends Base{

private static WebElement element = null;
public static String policynum,policyholder_name,postcode,start_date_policy,End_Date_Policy,Start_Date_Card,End_Date_Card;
	public static WebElement textarea(WebDriver driver,String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		element = driver.findElement(By.xpath("//input[@type='text' and @placeholder='"+fieldName+"']"));
		
		FileInputStream fis=new FileInputStream(""+project_path+"/Environment Setup/Test Input.xlsx");
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet("Input");
		policynum = sh.getRow(1).getCell(1).getStringCellValue();
		policyholder_name = sh.getRow(2).getCell(1).getStringCellValue();
		postcode = sh.getRow(3).getCell(1).getStringCellValue();
		start_date_policy =sh.getRow(4).getCell(1).getStringCellValue();
	    End_Date_Policy =sh.getRow(5).getCell(1).getStringCellValue();
	    Start_Date_Card =sh.getRow(6).getCell(1).getStringCellValue();
	    End_Date_Card = sh.getRow(7).getCell(1).getStringCellValue();

	    String name=fieldName ;

	    if(name.equalsIgnoreCase("Policy number")){
		element.sendKeys(policynum);
	    }
		else if(name.equalsIgnoreCase("Policyholder name")){
			element.sendKeys(policyholder_name);
			//PolicyDetailsPage.textarea(driver,fieldName).sendKeys(policyholder_name);
		}
		else if(name.equalsIgnoreCase("Postcode")){
			element.sendKeys(postcode);	
		}
	       
	    return element;
	}
	
	public static WebElement Search_button_click(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		driver.findElement(By.xpath("//button[text()='Search' and @type='button']")).click();
	    return element;

	}
	public static WebElement Select_address(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		Select objSelect =new Select(driver.findElement(By.id("mxui_widget_ReferenceSelector_0_input")));
		objSelect.selectByVisibleText("Zurich Life Assurance Co Ltd, The Zurich Centre 3000A, Parkway Whiteley, Fareham, PO15 7JY");
	    return element;

	}

	public static WebElement Start_Date_of_Insurence(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		driver.findElement(By.xpath("//input[@id='mxui_widget_DateInput_0_input']")).sendKeys(start_date_policy);	
		return element;
	}
	
	public static WebElement End_Date_of_Insurence(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		driver.findElement(By.xpath("//input[@id='mxui_widget_DateInput_1_input']")).sendKeys(End_Date_Policy);
		return element;
	}
	
	public static WebElement Start_Date_of_GreenCard(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		driver.findElement(By.xpath("//input[@id='mxui_widget_DateInput_2_input']")).sendKeys(Start_Date_Card);
		return element;
	}
	
	public static WebElement End_Date_of_GreenCard(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
		driver.findElement(By.xpath("//input[@id='mxui_widget_DateInput_3_input']")).sendKeys(End_Date_Card);
	    return element;
	}

	
	public static WebElement Continue_button_Home_Page(WebDriver driver) throws InterruptedException {
		element = driver.findElement(By.xpath("//button[text()='Continue' and @type='button']"));
		element.click();
		return element;
   }
}

