package pageObjects;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import resources.Base;
import resources.Log;

public class GreenCardConfirmationPage extends Base{
public static String project_path = System.getProperty("user.dir");

private static WebElement element = null;

public static WebElement referenceId_generated(WebDriver driver) throws InterruptedException {
element = driver.findElement(By.xpath("//p[contains(text(),'Your Green Card reference number is')]/following::h3[contains(text(),'GB/156/')]"));
referenceID=element.getText();
return element;


}

}

