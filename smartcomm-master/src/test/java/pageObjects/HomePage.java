package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import resources.Base;


public class HomePage extends Base{

private static WebElement element = null;
	
	public static WebElement button(WebDriver driver) throws InterruptedException {
		element = driver.findElement(By.xpath("//h3[contains(text(),'single')]/following-sibling::button[text()='Continue' and @type='button']"));
		element.click();
		return element;
		
	}
}
