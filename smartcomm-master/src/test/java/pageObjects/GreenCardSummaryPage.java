package pageObjects;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import resources.Base;


public class GreenCardSummaryPage extends Base{
public static String project_path = System.getProperty("user.dir");

private static WebElement element = null;

	public static WebElement submit_GreenCard_Button(WebDriver driver) throws InterruptedException {
	element = driver.findElement(By.xpath("//button[@type='button' and text()='Submit Green Card request']"));
	element.click();
	return element;
}
	public static WebElement radio_button_No_As_Docmosis_Doc_Generator(WebDriver driver) throws InterruptedException {
		element = driver.findElement(By.xpath("//input[@type='radio' and @value='false']"));
		element.click();
		return element;
       }

}

