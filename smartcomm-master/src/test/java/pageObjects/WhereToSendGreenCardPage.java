package pageObjects;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import resources.Base;



public class WhereToSendGreenCardPage extends Base{
public static String project_path = System.getProperty("user.dir");

private static WebElement element = null;

public static String email,confirm_email;
public static WebElement textarea(WebDriver driver,String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException  {
	element = driver.findElement(By.xpath("//input[@type='text' and @placeholder='"+fieldName+"']"));
	
	FileInputStream fis=new FileInputStream(""+project_path+"/Environment Setup/Test Input.xlsx");
	Workbook wb = WorkbookFactory.create(fis);
	Sheet sh = wb.getSheet("Input");
	email = sh.getRow(12).getCell(1).getStringCellValue();
	confirm_email = sh.getRow(13).getCell(1).getStringCellValue();
	
    String name=fieldName ;

    if(name.equalsIgnoreCase("Email")){
	element.sendKeys(email);
    }
	else if(name.equalsIgnoreCase("Confirm email")){
		element.sendKeys(confirm_email);
		//PolicyDetailsPage.textarea(driver,fieldName).sendKeys(policyholder_name);
	}
    return element;
}
	public static WebElement Requestor_Relationship(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
	Select objSelect =new Select(driver.findElement(By.id("mxui_widget_EnumSelect_1")));
	objSelect.selectByValue("The_Policyholder");
    return element;

}

	public static WebElement continue_button_WhereToSendGreenCardPage(WebDriver driver) throws InterruptedException {
	element = driver.findElement(By.xpath("//button[text()='Continue' and @type='button']"));
	element.click();
	return element;
}

}

