package pageObjects;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import resources.Base;



public class VehicleDetailsPage extends Base{
public static String project_path = System.getProperty("user.dir");

private static WebElement element = null;

public static String Registration_Number,Make_of_the_Vehicle;
public static WebElement textarea(WebDriver driver,String fieldName) throws EncryptedDocumentException, InvalidFormatException, IOException  {
	element = driver.findElement(By.xpath("//input[@type='text' and @placeholder='"+fieldName+"']"));
	
	FileInputStream fis=new FileInputStream(""+project_path+"/Environment Setup/Test Input.xlsx");
	Workbook wb = WorkbookFactory.create(fis);
	Sheet sh = wb.getSheet("Input");
	Registration_Number = sh.getRow(8).getCell(1).getStringCellValue();
	Make_of_the_Vehicle = sh.getRow(10).getCell(1).getStringCellValue();
	
    String name=fieldName ;

    if(name.equalsIgnoreCase("Registration number")){
	element.sendKeys(Registration_Number);
    }
	else if(name.equalsIgnoreCase("Vehicle make")){
		element.sendKeys(Make_of_the_Vehicle);
		//PolicyDetailsPage.textarea(driver,fieldName).sendKeys(policyholder_name);
	}
    return element;
}
	public static WebElement Vehicle_catgory(WebDriver driver) throws EncryptedDocumentException, InvalidFormatException, IOException  {
	Select objSelect =new Select(driver.findElement(By.id("mxui_widget_EnumSelect_0")));
	objSelect.selectByValue("A");
    return element;

}

	public static WebElement Continue_button_Vehicle_Page(WebDriver driver) throws InterruptedException {
	element = driver.findElement(By.xpath("//button[text()='Continue' and @type='button']"));
	element.click();
	return element;
}

}

