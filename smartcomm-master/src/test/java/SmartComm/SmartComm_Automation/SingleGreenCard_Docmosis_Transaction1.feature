@t
Feature: Single Green Card using Docmosis Generated Document
	

	Scenario: TC_001_Docmosis-Create a Single Green Card without manually entering the address
				Given Launch the application using Mendix application URL
				And Click on Continue button for Single Order in Home page
				
				And Enter the "Policy number" in Policy Details page
				And Enter the "Policyholder name" in Policy Details page
				And Enter the "Postcode" in Policy Details page
				And Click on Search button in Policy Details page
				And Select the Address in Policy Details page
				And Enter date picker for Start_Date_of_Insurence in Policy details Page
				And Enter date picker for End_Date_of_Insurence in Policy details Page
				And Enter date picker for Start_Date_of_GreenCard in Policy details Page
				And Enter date picker for End_Date_of_GreenCard in Policy details Page
				And Click on Continue button in Policy details Page
				
				And Enter "Registration number" in Vehicle details Page
        And Select Vehicle_Category from dropdown in Vehicle details Page
        And Enter "Vehicle make" in Vehicle details Page
        And Click on Continue in Vehicle details Page
        
        And Select Requestor Relationship from dropdown in Where to send GreenCard Page
        And Enter "Email" in where to send GreenCard Page
        And Enter "Confirm email" in where to send GreenCard Page
        And Click on Continue in where to send GreenCard Page
        
        And Select No from dropdown in Green Card summary Page
        And Click on Submit Green Card Request in Green Card summary Page
        And User receives the referenceID on successfull transaction
       