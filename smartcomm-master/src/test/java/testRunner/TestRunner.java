package testRunner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.xml.DOMConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import resources.Base;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
		features ="src/test/java/SmartComm/SmartComm_Automation",
		glue = {"stepDefinitions"},
		tags = "@t",
		junit = "--step-notifications",
		strict = true,
		dryRun = false
		)

public class TestRunner extends Base{ 
	
	public static String project_path = System.getProperty("user.dir");
	
	@BeforeClass
    public static void setUp() throws IOException {
		config();
		DOMConfigurator.configure("log4j.xml");		
		Runtime.getRuntime().exec("taskkill /im chromedriver.exe /f");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
		String timestamp = new SimpleDateFormat("yyyy_MM_dd_HHmmss").format(new Date());
		System.setProperty("extent.reporter.html.start", "true");
        System.setProperty("extent.reporter.html.config", "extent-config.xml");
        System.setProperty("extent.reporter.html.out", "Report/SmartComm_Automation_Test_Report_"+timestamp+".html");
    }

    @AfterClass
    public static void tearDown(){
    	
    }
	
}
